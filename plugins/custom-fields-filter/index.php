<?php
/**
 * Plugin Name
 *
 * @package           CustomFieldsFilter
 * @author            Mustafa Jamal
 * @copyright         2019 Mustafa Jamal Logics Buffer
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Custom Fields Filter
 * Plugin URI:        https://logicsbuffer.com
 * Description:       Filters Products by fields
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa Jamal
 * Author URI:        https://mustafajamal.logicsbuffer.com/
 * Text Domain:       custom-fields
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */


function CustomFieldsFilter($atts='') {


$value = shortcode_atts( array(
        'key' => 'memory_built_in',
        'value' => ' 3GB RAM',
        'title' => 'Products',
    ), $atts );

$params = array(
    'post_type' => 'product',
    'meta_query' => array(
        array('key' => $value['key'], //meta key name here
              'value' => $value['value'], 
              'compare' => 'LIKE',
        )
    ),  
    'posts_per_page' => 25 

);
$wc_query = new WP_Query($params);


ob_start();
?>
<style type="text/css">.cffproductsshow img.attachment-thumbnail.size-thumbnail {width: 30%;max-height: 68px;}</style>
<div id="" class="title-wrapper woodmart-title-color-primary woodmart-title-style-underlined woodmart-title-width-100 text-left woodmart-title-size-default ">
	<div class="liner-continer"> <span class="left-line"></span><h4 class="woodmart-title-container title  woodmart-font-weight-"><?php echo $value['title']; ?></h4> <span class="right-line"></span></div>
</div>
<div class="cffproductsshow products elements-grid align-items-start row woodmart-products-holder  woodmart-spacing-20 grid-columns-6  products-bordered-grid title-line-two">


<?php

if( $wc_query->have_posts() ) {
 global $post, $product;
  while( $wc_query->have_posts() ) {
  	
    $wc_query->the_post();
    $wc_query->post_name;
    $price = get_post_meta( get_the_ID(), '_price', true );
       ?>
       	<div class="product-grid-item product col-6 col-sm-4 col-md-3 col-lg-2"><a href="<?php the_permalink(); ?>" >
            <?php echo get_the_post_thumbnail($post_name->post->ID, 'thumbnail'); ?> 
            <h3 class="product-title"><?php the_title(); ?></h3>
            <span class="price"><?php echo wc_price( $price ); ?></span>
               
        </a></div><?php
        
  } // end while
} // end if

 else 
{ echo "nothing found"; }

wp_reset_postdata();
?></div><?php
return ob_get_clean();

}
add_shortcode('cff', 'CustomFieldsFilter');


//Price Filter Shortcode

function CustomFieldsFilterPrice($atts) {


$value = shortcode_atts( array(
        'key' => '_regular_price',
        'min' => '10000',
        'max' => '80000',
        'category' => '', 
        'limit' => '', 
        'title' => 'Products',
    ), $atts );

$params = array(
    'post_type' => 'product',
    'meta_query' => array(
        array('key' => $value['key'], //meta key name here
              'value' => array($value['min'],$value['max']),
              'compare' => 'BETWEEN',
              'type'      => 'numeric'
        )
    ), 
    'posts_per_page' => $value['limit'] 

);
$wc_query = new WP_Query($params);

ob_start();
?>
<style type="text/css">.cffproductsshow img.attachment-thumbnail.size-thumbnail {width: 20%;max-height: 68px;}</style>
<div id="" class="title-wrapper woodmart-title-color-primary woodmart-title-style-underlined woodmart-title-width-100 text-left woodmart-title-size-default ">
	<div class="liner-continer"> <span class="left-line"></span><h4 class="woodmart-title-container title  woodmart-font-weight-"><?php echo $value['title']; ?></h4> <span class="right-line"></span></div>
</div>
<div class="cffproductsshow products elements-grid align-items-start row woodmart-products-holder  woodmart-spacing-20 grid-columns-6  products-bordered-grid title-line-two">


<?php
if( $wc_query->have_posts() ) {
 
  while( $wc_query->have_posts() ) {
  	global $post, $product;
    $wc_query->the_post();
    $wc_query->post_name;
    if ($product->regular_price != 0) {
    
       ?>
       	<div class="product-grid-item product col-6 col-sm-4 col-md-3 col-lg-2"><a href="<?php the_permalink(); ?>" >
            <?php echo get_the_post_thumbnail($post_name->post->ID, 'thumbnail'); ?> 
            <h3 class="product-title"><?php the_title(); ?></h3>
            <span class="price">Rs <?php echo number_format($product->regular_price); ?></span>
               
        </a></div><?php
    }            
  } // end while
} // end if

 else 
{ echo "nothing found"; }

wp_reset_postdata();
?></div><?php
return ob_get_clean();

}
add_shortcode('cffp', 'CustomFieldsFilterPrice');
