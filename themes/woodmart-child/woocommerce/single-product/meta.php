<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>

<div class="product_meta">

<?php
$cam_main = get_field( "cam_main" );
$camera = explode(',',$cam_main);
$processor_cpu = get_field( "processor_cpu" );
$processor = explode(',',$processor_cpu);
$memory_built_in = get_field( "memory_built_in" );
$memory = explode(',',$memory_built_in);
$battery = get_field( "battery" );
$battery_short = explode(',',$battery);

?>

<div id="specs_overview">
	<div class="row">
		<div class="col col-sm-6 col-sm-offset-2 specs_cols camera">
			<span class="specs_icon"><i class="fa fa-camera"></i></span> <span class="info"><?php echo $camera[0];?></span>
		</div>
		<div class="col col-sm-6 col-sm-offset-2 specs_cols memory"><span class="specs_icon"><i class="fas fa-sd-card"></i></span> <span class="info"><?php echo $memory[0];?></span></div>
	</div>
	<div class="row">
		<div class="col col-sm-6 col-sm-offset-2 specs_cols processor"><span class="specs_icon"><i class="fa fa-microchip"></i></span> <span class="info"><?php echo $processor[0];?></span></div>
		<div class="col col-sm-6 col-sm-offset-2 specs_cols battery"><span class="specs_icon"><i class="fa fa-battery-three-quarters"></i></span> <span class="info"><?php echo $battery_short[0];?></span></div>
	</div>
</div>

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
		<?php $sku = $product->get_sku(); ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo true == $sku ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo wc_get_product_category_list( $product->get_id(), '<span class="meta-sep">,</span> ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php echo wc_get_product_tag_list( $product->get_id(), '<span class="meta-sep">,</span> ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php
		//Build
		$build_os = get_field( "build_os" );
		$build_ui = get_field( "build_ui" );
		$build_dimensions = get_field( "build_dimensions" );
		$build_weight = get_field( "build_weight" );
		$build_sim = get_field( "build_sim" );
		$build_colors = get_field( "build_colors" );

		if( $build_os ) {
		    $build_os = $build_os;
		} else {
		    $build_os = 'N/A';
		} 
		if( $build_ui ) {
		    $build_ui = $build_ui;
		} else {
		    $build_ui = 'N/A';
		} 
		if( $build_dimensions ) {
		    $build_dimensions = $build_dimensions;
		} else {
		    $build_dimensions = 'N/A';
		} 
		if( $build_weight ) {
		    $build_weight = $build_weight;
		} else {
		    $build_weight = 'N/A';
		}  
		if( $build_sim ) {
		    $build_sim = $build_sim;
		} else {
		    $build_sim = 'N/A';
		}  
		if( $build_colors ) {
		    $build_colors = $build_colors;
		} else {
		    $build_colors = 'N/A';
		}
		
		//Frequency
		$frequency_2g_band = get_field( "frequency_2g_band" );
		$frequency_3g_band = get_field( "frequency_3g_band" );
		$frequency_4g_band = get_field( "frequency_4g_band" );
		$frequency_5g_band = get_field( "frequency_5g_band" );

		if( $frequency_2g_band ) {
		    $frequency_2g_band = $frequency_2g_band;
		} else {
		    $frequency_2g_band = 'N/A';
		}
		if( $frequency_3g_band ) {
		    $frequency_3g_band = $frequency_3g_band;
		} else {
		    $frequency_3g_band = 'N/A';
		} 
		if( $frequency_4g_band ) {
		    $frequency_4g_band = $frequency_4g_band;
		} else {
		    $frequency_4g_band = 'N/A';
		} 
		if( $frequency_5g_band ) {
		    $frequency_5g_band = $frequency_5g_band;
		} else {
		    $frequency_5g_band = 'N/A';
		} 

		//Processor
		$processor_cpu = get_field( "processor_cpu" );
		$processor_chipset = get_field( "processor_chipset" );

		if( $processor_cpu ) {
		    $processor_cpu = $processor_cpu;
		} else {
		    $processor_cpu = 'N/A';
		}
		if( $processor_chipset ) {
		    $processor_chipset = $processor_chipset;
		} else {
		    $processor_chipset = 'N/A';
		} 
		

		//Display
		$display_technology = get_field( "display_technology" );
		$display_size = get_field( "display_size" );
		$display_resolution = get_field( "display_resolution" );
		$display_protection = get_field( "display_protection" );
		
		if( $display_technology ) {
		    $display_technology = $display_technology;
		} else {
		    $display_technology = 'N/A';
		}
		if( $display_size ) {
		    $display_size = $display_size;
		} else {
		    $display_size = 'N/A';
		}
		if( $display_resolution ) {
		    $display_resolution = $display_resolution;
		} else {
		    $display_resolution = 'N/A';
		}
		if( $display_protection ) {
		    $display_protection = $display_protection;
		} else {
		    $display_protection = 'N/A';
		}


		//Memory
		$memory_built_in = get_field( "memory_built_in" );
		$memory_card = get_field( "memory_card" );

		if( $memory_built_in ) {
		    $memory_built_in = $memory_built_in;
		} else {
		    $memory_built_in = 'N/A';
		}
		if( $memory_card ) {
		    $memory_card = $memory_card;
		} else {
		    $memory_card = 'N/A';
		}

		//Cam
		$cam_main = get_field( "cam_main" );
		$cam_features = get_field( "cam_features" );
		$cam_front = get_field( "cam_front" );

		if( $cam_main ) {
		    $cam_main = $cam_main;
		} else {
		    $cam_main = 'N/A';
		}
		if( $cam_features ) {
		    $cam_features = $cam_features;
		} else {
		    $cam_features = 'N/A';
		}
		if( $cam_front ) {
		    $cam_front = $cam_front;
		} else {
		    $cam_front = 'N/A';
		}

		//Connectivity
		$connectivity_wlan = get_field( "connectivity_wlan" );
		$connectivity_bluetooth = get_field( "connectivity_bluetooth" );
		$connectivity_gps = get_field( "connectivity_gps" );
		$connectivity_usb = get_field( "connectivity_usb" );
		$connectivity_nfc = get_field( "connectivity_nfc" );
		$connectivity_data = get_field( "connectivity_data" );
		

		if( $connectivity_wlan ) {
		    $connectivity_wlan = $connectivity_wlan;
		} else {
		    $connectivity_wlan = 'N/A';
		}
		if( $connectivity_bluetooth ) {
		    $connectivity_bluetooth = $connectivity_bluetooth;
		} else {
		    $connectivity_bluetooth = 'N/A';
		}
		if( $connectivity_gps ) {
		    $connectivity_gps = $connectivity_gps;
		} else {
		    $connectivity_gps = 'N/A';
		}
		if( $connectivity_usb ) {
		    $connectivity_usb = $connectivity_usb;
		} else {
		    $connectivity_usb = 'N/A';
		}
		if( $connectivity_nfc ) {
		    $connectivity_nfc = $connectivity_nfc;
		} else {
		    $connectivity_nfc = 'N/A';
		}
		if( $connectivity_data ) {
		    $connectivity_data = $connectivity_data;
		} else {
		    $connectivity_data = 'N/A';
		}


		//Features
		$features_sensors = get_field( "features_sensors" );
		$features_audio = get_field( "features_audio" );
		$features_browser = get_field( "features_browser" );
		$features_messaging = get_field( "features_messaging" );
		$features_games = get_field( "features_games" );
		$features_torch = get_field( "features_torch" );
		$features_extra = get_field( "features_extra" );

		if( $features_sensors ) {
		    $features_sensors = $features_sensors;
		} else {
		    $features_sensors = 'N/A';
		}
		if( $features_audio ) {
		    $features_audio = $features_audio;
		} else {
		    $features_audio = 'N/A';
		} 
		if( $features_browser ) {
		    $features_browser = $features_browser;
		} else {
		    $features_browser = 'N/A';
		} 
		if( $features_messaging ) {
		    $features_messaging = $features_messaging;
		} else {
		    $features_messaging = 'N/A';
		} 
		if( $features_games ) {
		    $features_games = $features_games;
		} else {
		    $features_games = 'N/A';
		} 
		if( $features_torch ) {
		    $features_torch = $features_torch;
		} else {
		    $features_torch = 'N/A';
		} 
		if( $features_extra ) {
		    $features_extra = $features_extra;
		} else {
		    $features_extra = 'N/A';
		} 

		//Battery
		$battery = get_field( "battery" );
		
		if( $battery ) {
		    $battery = $battery;
		} else {
		    $battery = 'N/A';
		} 
	?>
<style type="text/css">
	table.table.table-bordered, table.table.table-bordered th, table.table.table-bordered td {
    	border: 1px solid #e6e6e6 !important;
    	padding: 6px;
	}
	.product-image-wrap figure.woocommerce-product-gallery__image {
	    text-align: center;
	}
	.product-image-wrap img.wp-post-image.wp-post-image {
	    width: 50%;
	    margin: 0 auto !important;
	    text-align: center;
	    display: inline-block !important;
	    float: none !important;
	}
</style>
<div class="table-responsive-sm">
  <table class="table table-bordered table-hover">
    
    <tbody>
      <!-- Build -->
      <tr>
        <th rowspan="6" class="specs_name text-center" scope="row">Build</th>
        <th align="left">OS</th>
        <td colspan="2"><?php echo $build_os; ?></td>        
      </tr>
       <tr>
        <th align="left">UI</th>
        <td colspan="2"><?php echo $build_ui; ?></td>        
      </tr>
      <tr>
        <th align="left">Dimentions</th>
        <td colspan="2"><?php echo $build_dimensions; ?></td>        
      </tr>
      <tr>
        <th align="left">Weight</th>
        <td colspan="2"><?php echo $build_weight; ?></td>        
      </tr>
     <tr>
        <th align="left">Sim</th>
        <td colspan="2"><?php echo $build_sim; ?></td>        
      </tr>
      <tr>
        <th align="left">Colors</th>
        <td colspan="2"><?php echo $build_colors; ?></td>        
      </tr>

      <!-- Display -->
      <tr>
        <th rowspan="4" class="specs_name text-center" scope="row">Display</th>
        <th align="left">Display Technology</th>
        <td colspan="2"><?php echo $display_technology; ?></td>        
      </tr>
       <tr>
        <th align="left">Display Size</th>
        <td colspan="2"><?php echo $display_size; ?></td>        
      </tr>
      <tr>
        <th align="left">Display Resolution</th>
        <td colspan="2"><?php echo $display_resolution; ?></td>        
      </tr> 
      <tr>
        <th align="left">Display Protection</th>
        <td colspan="2"><?php echo $display_protection; ?></td>        
      </tr> 

      <!-- Frequency -->
      <tr>
        <th rowspan="4" class="specs_name text-center" scope="row">Frequency</th>
        <th align="left">Frequency 2G Band</th>
        <td colspan="2"><?php echo $frequency_2g_band; ?></td>        
      </tr>
      <tr>
        <th align="left">Frequency 3G Band</th>
        <td colspan="2"><?php echo $frequency_3g_band; ?></td>        
      </tr>
      <tr>
        <th align="left">Frequency 4G Band</th>
        <td colspan="2"><?php echo $frequency_4g_band; ?></td>        
      </tr>  
      <tr>
        <th align="left">Frequency 5G Band</th>
        <td colspan="2"><?php echo $frequency_5g_band; ?></td>        
      </tr> 

      <!-- Processor -->
      <tr>
        <th rowspan="2" class="specs_name text-center" scope="row">Processor</th>
        <th align="left">Processor CPU</th>
        <td colspan="2"><?php echo $processor_cpu; ?></td>        
      </tr>
       <tr>
        <th align="left">Processor Chipset</th>
        <td colspan="2"><?php echo $processor_chipset; ?></td>        
      </tr>

      <!-- Memory -->
      <tr>
        <th rowspan="2" class="specs_name text-center" scope="row">Memory</th>
        <th align="left">Memory Built in</th>
        <td colspan="2"><?php echo $memory_built_in; ?></td>        
      </tr>
       <tr>
        <th align="left">Memory Card</th>
        <td colspan="2"><?php echo $memory_card; ?></td>        
      </tr>

      <!-- Cam -->
      <tr>
        <th rowspan="3" class="specs_name text-center" scope="row">Cam</th>
        <th align="left">Cam Main</th>
        <td colspan="2"><?php echo $cam_main; ?></td>        
      </tr>
      <tr>
        <th align="left">Cam Features</th>
        <td colspan="2"><?php echo $cam_features; ?></td>        
      </tr>
      <tr>
        <th align="left">Cam Front</th>
        <td colspan="2"><?php echo $cam_front; ?></td>        
      </tr>

      
      <!-- Connectivity -->
      <tr>
        <th rowspan="7" class="specs_name text-center" scope="row">Connectivity</th>
        <th align="left">WLAN</th>
        <td colspan="2"><?php echo $connectivity_wlan; ?></td>        
      </tr>
       <tr>
        <th align="left">Bluetooth</th>
        <td colspan="2"><?php echo $connectivity_bluetooth; ?></td>        
      </tr>
       <tr>
        <th align="left">GPS</th>
        <td colspan="2"><?php echo $connectivity_gps; ?></td>        
      </tr>
       <tr>
        <th align="left">USB</th>
        <td colspan="2"><?php echo $connectivity_usb; ?></td>        
      </tr>
       <tr>
        <th align="left">Games</th>
        <td colspan="2"><?php echo $features_games; ?></td>        
      </tr>
       <tr>
        <th align="left">Torch</th>
        <td colspan="2"><?php echo $features_torch; ?></td>        
      </tr>
       <tr>
        <th align="left">Extra</th>
        <td colspan="2"><?php echo $features_extra; ?></td>        
      </tr>

      <!-- Features -->
      <tr>
        <th rowspan="4" class="specs_name text-center" scope="row">Features</th>
        <th align="left">Sensors</th>
        <td colspan="2"><?php echo $features_sensors; ?></td>        
      </tr>
      <tr>
        <th align="left">Audio</th>
        <td colspan="2"><?php echo $features_audio; ?></td>        
      </tr>
      <tr>
        <th align="left">Browser</th>
        <td colspan="2"><?php echo $features_browser; ?></td>        
      </tr>
      <tr>
        <th align="left">Messaging</th>
        <td colspan="2"><?php echo $features_messaging; ?></td>        
      </tr>

      <!-- Battery -->
      <tr>
        <th rowspan="1" class="specs_name text-center" scope="row">Battery</th>
        <th align="left">Battery</th>
        <td colspan="2"><?php echo $battery; ?></td>        
      </tr>      
     
    </tbody>
  </table>
</div>



	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
