<?php
/**
 * WooCommerce compare functions
 *
 * @package woodmart
 */

if ( ! defined( 'WOODMART_THEME_DIR' ) ) {
	exit( 'No direct script access allowed' );
}


if ( ! function_exists( 'woodmart_compare_shortcode' ) ) {
	/**
	 * WooCommerce compare page shortcode.
	 *
	 * @since 3.3
	 */
	function woodmart_compare_shortcode() {
		if ( ! woodmart_woocommerce_installed() ) return;
		ob_start();
		?>
			<?php woodmart_get_compared_products_table(); ?>
		<?php
		return ob_get_clean();
	}
}


if ( ! function_exists( 'woodmart_add_to_compare' ) ) {
	/**
	 * Add product to comapre
	 *
	 * @since 3.3
	 */
	function woodmart_add_to_compare() {
		$id = sanitize_text_field( $_GET['id'] );

		if ( defined( 'ICL_SITEPRESS_VERSION' ) && function_exists( 'wpml_object_id_filter' ) ) {
			global $sitepress;
			$id = wpml_object_id_filter( $id, 'product', true, $sitepress->get_default_language() );
		}

		$cookie_name = woodmart_compare_cookie_name();

		if ( woodmart_is_product_in_compare( $id ) ) {
			woodmart_compare_json_response();
		}

		$products = woodmart_get_compared_products();

		$products[] = $id;

		setcookie( $cookie_name, json_encode( $products ), 0, COOKIEPATH, COOKIE_DOMAIN, false, false );

		$_COOKIE[$cookie_name] = json_encode( $products );

		woodmart_compare_json_response();
	}

	add_action( 'wp_ajax_woodmart_add_to_compare', 'woodmart_add_to_compare' );
	add_action( 'wp_ajax_nopriv_woodmart_add_to_compare', 'woodmart_add_to_compare' );
}


if ( ! function_exists( 'woodmart_add_to_compare_btn' ) ) {
	/**
	 * Add product to comapre button
	 *
	 * @since 3.3
	 */
	function woodmart_add_to_compare_btn() {
		global $product;
		echo '<div class="woodmart-compare-btn product-compare-button"><a href="' . esc_url( woodmart_get_compare_page_url() ) . '" data-added-text="' . __('Compare products', 'woodmart') . '" data-id="' . esc_attr( $product->get_id() ) . '">' . esc_html_x( 'Compare', 'add_button', 'woodmart' ) . '</a></div>';
	}
}


if ( ! function_exists( 'woodmart_compare_json_response' ) ) {
	/**
	 * Compare JSON reponse.
	 *
	 * @since 3.3
	 */
	function woodmart_compare_json_response() {
		$count = 0;
		$products = woodmart_get_compared_products();

		ob_start();

		woodmart_get_compared_products_table();

		$table_html = ob_get_clean();

		if ( is_array( $products ) ) {
			$count = count( $products );
		}

		wp_send_json( array(
			'count' => $count,
			'table' => $table_html,
		) );
	}
}

if ( ! function_exists( 'woodmart_get_compare_page_url' ) ) {
	/**
	 * Get compare page ID.
	 *
	 * @since 3.3
	 */
	function woodmart_get_compare_page_url() {
		$page_id = woodmart_get_opt( 'compare_page' );

		if ( defined( 'ICL_SITEPRESS_VERSION' ) && function_exists( 'wpml_object_id_filter' ) ) {
			$page_id = wpml_object_id_filter( $page_id, 'page', true );
		}

		return get_permalink( $page_id );
	}
}


if ( ! function_exists( 'woodmart_remove_from_compare' ) ) {
	/**
	 * Add product to comapre
	 *
	 * @since 3.3
	 */
	function woodmart_remove_from_compare() {
		$id = sanitize_text_field( $_GET['id'] );

		if ( defined( 'ICL_SITEPRESS_VERSION' ) && function_exists( 'wpml_object_id_filter' ) ) {
			global $sitepress;
			$id = wpml_object_id_filter( $id, 'product', true, $sitepress->get_default_language() );
		}

		$cookie_name = woodmart_compare_cookie_name();

		if ( ! woodmart_is_product_in_compare( $id ) ) {
			woodmart_compare_json_response();
		}

		$products = woodmart_get_compared_products();

		foreach ( $products as $k => $product_id ) {
			if ( intval( $id ) == $product_id ) {
				unset( $products[ $k ] );
			}
		}

		if ( empty( $products ) ) {
			setcookie( $cookie_name, false, 0, COOKIEPATH, COOKIE_DOMAIN, false, false );
			$_COOKIE[$cookie_name] = false;
		} else {
			setcookie( $cookie_name, json_encode( $products ), 0, COOKIEPATH, COOKIE_DOMAIN, false, false );
			$_COOKIE[$cookie_name] = json_encode( $products );
		}

		woodmart_compare_json_response();
	}

	add_action( 'wp_ajax_woodmart_remove_from_compare', 'woodmart_remove_from_compare' );
	add_action( 'wp_ajax_nopriv_woodmart_remove_from_compare', 'woodmart_remove_from_compare' );
}


if ( ! function_exists( 'woodmart_is_product_in_compare' ) ) {
	/**
	 * Is product in compare
	 *
	 * @since 3.3
	 */
	function woodmart_is_product_in_compare( $id ) {
		$list = woodmart_get_compared_products();

		return in_array( $id, $list, true );
	}
}


if ( ! function_exists( 'woodmart_get_compare_count' ) ) {
	/**
	 * Get compare number.
	 *
	 * @since 3.3
	 */
	function woodmart_get_compare_count() {
		$count = 0;
		$products = woodmart_get_compared_products();

		if ( is_array( $products ) ) {
			$count = count( $products );
		}

		return $count;
	}
}


if ( ! function_exists( 'woodmart_compare_cookie_name' ) ) {
	/**
	 * Get compare cookie namel.
	 *
	 * @since 3.3
	 */
	function woodmart_compare_cookie_name() {
		$name = 'woodmart_compare_list';

        if ( is_multisite() ) $name .= '_' . get_current_blog_id();

        return $name;

	}
}


if ( ! function_exists( 'woodmart_get_compared_products' ) ) {
	/**
	 * Get compared products IDs array
	 *
	 * @since 3.3
	 */
	function woodmart_get_compared_products() {
		$cookie_name = woodmart_compare_cookie_name();
        return isset( $_COOKIE[ $cookie_name ] ) ? json_decode( wp_unslash( $_COOKIE[ $cookie_name ] ), true ) : array();
	}
}

if ( ! function_exists( 'woodmart_is_products_have_field' ) ) {
	/**
	 * Checks if the products have such a field.
	 *
	 * @since 3.4
	 */
	function woodmart_is_products_have_field( $field_id, $products ) {
		foreach ( $products as $product_id => $product ) {
			if ( isset( $product[ $field_id ] ) && ( ! empty( $product[ $field_id ] ) && '-' !== $product[ $field_id ] && 'N/A' !== $product[ $field_id ] ) ) {
				return true;
			}
		}

		return false;
	}
}

if ( ! function_exists( 'woodmart_get_compared_products_table' ) ) {
	/**
	 * Get compared products data table HTML
	 *
	 * @since 3.3
	 */
	function woodmart_get_compared_products_table() {
		$products = woodmart_get_compared_products_data();
		$fields = woodmart_get_compare_fields();
		$empty_compare_text = woodmart_get_opt( 'empty_compare_text' );

		?>
		<div class="woodmart-compare-table">
			<?php
			if ( ! empty( $products ) ) {
				array_unshift( $products, array() );
				foreach ( $fields as $field_id => $field ) {
					if ( ! woodmart_is_products_have_field( $field_id, $products ) ) {
						continue;
					}
					?>
						<div class="woodmart-compare-row compare-<?php echo esc_attr( $field_id ); ?>">
							<?php foreach ( $products as $product_id => $product ) : ?>
								<?php if ( ! empty( $product ) ) : ?>
									<div class="woodmart-compare-col compare-value" data-title="<?php echo esc_attr( $field ); ?>">
										<?php woodmart_compare_display_field( $field_id, $product ); ?>
									</div>
								<?php else: ?>
									<div class="woodmart-compare-col compare-field">
										<?php echo esc_html( $field ); ?>
									</div>
								<?php endif; ?>

							<?php endforeach ?>
						</div>
					<?php
				}	
			} else {
				?>
					<p class="woodmart-empty-compare">
						<?php esc_html_e('Compare list is empty.', 'woodmart'); ?>
					</p>
					<?php if ( $empty_compare_text ) : ?>
						<div class="woodmart-empty-page-text"><?php echo wp_kses( $empty_compare_text, array('p' => array(), 'h1' => array(), 'h2' => array(), 'h3' => array(), 'strong' => array(), 'em' => array(), 'span' => array(), 'div' => array() , 'br' => array()) ); ?></div>
					<?php endif; ?>
					<p class="return-to-shop">
						<a class="button" href="<?php echo esc_url( wc_get_page_permalink( 'shop' ) ); ?>">
							<?php esc_html_e( 'Return to shop', 'woodmart' ); ?>
						</a>
					</p>
				<?php
			}

			?>
			<div class="compare-loader"></div>
		</div>
		<?php
	}
}


if ( ! function_exists( 'woodmart_get_compare_fields' ) ) {
	/**
	 * Get compare fields data.
	 *
	 * @since 3.3
	 */
	function woodmart_get_compare_fields() {
		$fields = array(
			'basic' => ''
		);

		$fields_settings = woodmart_get_opt('fields_compare');
		
		if ( class_exists( 'XTS\Options' ) && count( $fields_settings ) > 1 ) {
			$fields_labels = woodmart_compare_available_fields( true );

			foreach ( $fields_settings as $field ) {
				if ( isset( $fields_labels [ $field ] ) ) {
					$fields[ $field ] = $fields_labels [ $field ]['name'];
				}
			}

			return $fields;
		}

		if ( isset( $fields_settings['enabled'] ) && count( $fields_settings['enabled'] ) > 1 ) {
			$fields = $fields + $fields_settings['enabled'];
			unset( $fields['placebo'] );
		}


		return $fields;
	}
}


if ( ! function_exists( 'woodmart_compare_display_field' ) ) {
	/**
	 * Get compare fields data.
	 *
	 * @since 3.3
	 */
	function woodmart_compare_display_field( $field_id, $product ) {

		$type = $field_id;

		if ( 'pa_' === substr( $field_id, 0, 3 ) ) {
			$type = 'attribute';
		}
		
		switch ( $type ) {
			case 'basic':
				echo '<div class="compare-basic-content">';
					echo '<a href="#" class="woodmart-compare-remove woodmart-button-remove" data-id="' . esc_attr( $product['id'] ) . '"><span class="remove-loader"></span>' . esc_html__( 'Remove', 'woodmart' ) . '</a>';
					echo '<a class="product-image" href="' . get_permalink( $product['id'] ) . '">' . $product['basic']['image']. '</a>';
					echo '<a class="product-title" href="' . get_permalink( $product['id'] ) . '">' . $product['basic']['title']. '</a>';
					echo wp_kses_post( $product['basic']['rating'] );
					echo '<div class="price">';
						echo wp_kses_post( $product['basic']['price'] );
					echo '</div>';
					if ( ! woodmart_get_opt( 'catalog_mode' ) ) {
						//echo apply_filters( 'woodmart_compare_add_to_cart_btn', $product['basic']['add_to_cart'] );
					}
					
					//Custom Features Get
					$build_os = get_post_meta( $product['id'], 'build_os', true );
					$build_ui = get_post_meta( $product['id'], 'build_ui', true );
					$build_dimensions = get_post_meta( $product['id'], 'build_dimensions', true );
					$build_weight = get_post_meta( $product['id'], 'build_weight', true );
					$build_sim = get_post_meta( $product['id'], 'build_sim', true );
					$build_colors = get_post_meta( $product['id'], 'build_colors', true );
					$frequency_2g_band = get_post_meta( $product['id'], 'frequency_2g_band', true );
					$frequency_3g_band = get_post_meta( $product['id'], 'frequency_3g_band', true );
					$frequency_4g_band = get_post_meta( $product['id'], 'frequency_4g_band', true );
					$processor_cpu = get_post_meta( $product['id'], 'processor_cpu', true );
					$processor_chipset = get_post_meta( $product['id'], 'processor_chipset', true );
					$display_technology = get_post_meta( $product['id'], 'display_technology', true );
					$display_size = get_post_meta( $product['id'], 'display_size', true );
					$display_resolution = get_post_meta( $product['id'], 'display_resolution', true );
					$display_protection = get_post_meta( $product['id'], 'display_protection', true );
					$memory_built_in = get_post_meta( $product['id'], 'memory_built_in', true );
					$memory_card = get_post_meta( $product['id'], 'memory_card', true );
					$cam_main = get_post_meta( $product['id'], 'cam_main', true );
					$cam_features = get_post_meta( $product['id'], 'cam_features', true );
					$cam_front = get_post_meta( $product['id'], 'cam_front', true );
					$connectivity_wlan = get_post_meta( $product['id'], 'connectivity_wlan', true );
					$connectivity_bluetooth = get_post_meta( $product['id'], 'connectivity_bluetooth', true );
					$connectivity_gps = get_post_meta( $product['id'], 'connectivity_gps', true );
					$connectivity_usb = get_post_meta( $product['id'], 'connectivity_usb', true );
					$connectivity_nfc = get_post_meta( $product['id'], 'connectivity_nfc', true );
					$connectivity_data = get_post_meta( $product['id'], 'connectivity_data', true );
					$features_sensors = get_post_meta( $product['id'], 'features_sensors', true );
					$features_audio = get_post_meta( $product['id'], 'features_audio', true );
					$features_browser = get_post_meta( $product['id'], 'features_browser', true );
					$features_messaging = get_post_meta( $product['id'], 'features_messaging', true );
					$features_games = get_post_meta( $product['id'], 'features_games', true );
					$features_torch = get_post_meta( $product['id'], 'features_torch', true );
					$features_extra = get_post_meta( $product['id'], 'features_extra', true );
					$battery = get_post_meta( $product['id'], 'battery', true );

					if( $build_os ) {
					    $build_os = $build_os;
					} else {
					    $build_os = 'N/A';
					} 
					if( $build_ui ) {
					    $build_ui = $build_ui;
					} else {
					    $build_ui = 'N/A';
					} 
					if( $build_dimensions ) {
					    $build_dimensions = $build_dimensions;
					} else {
					    $build_dimensions = 'N/A';
					} 
					if( $build_weight ) {
					    $build_weight = $build_weight;
					} else {
					    $build_weight = 'N/A';
					}  
					if( $build_sim ) {
					    $build_sim = $build_sim;
					} else {
					    $build_sim = 'N/A';
					}  
					if( $build_colors ) {
					    $build_colors = $build_colors;
					} else {
					    $build_colors = 'N/A';
					}			
					if( $processor_cpu ) {
					    $processor_cpu = $processor_cpu;
					} else {
					    $processor_cpu = 'N/A';
					}
					if( $processor_chipset ) {
					    $processor_chipset = $processor_chipset;
					} else {
					    $processor_chipset = 'N/A';
					} 
	
					if( $display_technology ) {
					    $display_technology = $display_technology;
					} else {
					    $display_technology = 'N/A';
					}
					if( $display_size ) {
					    $display_size = $display_size;
					} else {
					    $display_size = 'N/A';
					}
					if( $display_resolution ) {
					    $display_resolution = $display_resolution;
					} else {
					    $display_resolution = 'N/A';
					}
					if( $display_protection ) {
					    $display_protection = $display_protection;
					} else {
					    $display_protection = 'N/A';
					}

					if( $memory_built_in ) {
					    $memory_built_in = $memory_built_in;
					} else {
					    $memory_built_in = 'N/A';
					}
					if( $memory_card ) {
					    $memory_card = $memory_card;
					} else {
					    $memory_card = 'N/A';
					}
					if( $cam_main ) {
					    $cam_main = $cam_main;
					} else {
					    $cam_main = 'N/A';
					}
					if( $cam_features ) {
					    $cam_features = $cam_features;
					} else {
					    $cam_features = 'N/A';
					}
					if( $cam_front ) {
					    $cam_front = $cam_front;
					} else {
					    $cam_front = 'N/A';
					}
					if( $connectivity_wlan ) {
								    $connectivity_wlan = $connectivity_wlan;
					} else {
					    $connectivity_wlan = 'N/A';
					}
					if( $connectivity_bluetooth ) {
					    $connectivity_bluetooth = $connectivity_bluetooth;
					} else {
					    $connectivity_bluetooth = 'N/A';
					}
					if( $connectivity_gps ) {
					    $connectivity_gps = $connectivity_gps;
					} else {
					    $connectivity_gps = 'N/A';
					}
					if( $connectivity_usb ) {
					    $connectivity_usb = $connectivity_usb;
					} else {
					    $connectivity_usb = 'N/A';
					}
					if( $connectivity_nfc ) {
					    $connectivity_nfc = $connectivity_nfc;
					} else {
					    $connectivity_nfc = 'N/A';
					}
					if( $connectivity_data ) {
					    $connectivity_data = $connectivity_data;
					} else {
					    $connectivity_data = 'N/A';
					}
					if( $features_sensors ) {
					    $features_sensors = $features_sensors;
					} else {
					    $features_sensors = 'N/A';
					}
					if( $features_audio ) {
					    $features_audio = $features_audio;
					} else {
					    $features_audio = 'N/A';
					} 
					if( $features_browser ) {
					    $features_browser = $features_browser;
					} else {
					    $features_browser = 'N/A';
					} 
					if( $features_messaging ) {
					    $features_messaging = $features_messaging;
					} else {
					    $features_messaging = 'N/A';
					} 
					if( $features_games ) {
					    $features_games = $features_games;
					} else {
					    $features_games = 'N/A';
					} 
					if( $features_torch ) {
					    $features_torch = $features_torch;
					} else {
					    $features_torch = 'N/A';
					} 
					if( $features_extra ) {
					    $features_extra = $features_extra;
					} else {
					    $features_extra = 'N/A';
					}
					if( $battery ) {
					    $battery = $battery;
					} else {
					    $battery = 'N/A';
					}  

					echo '<div class="compare_features"><strong>OS: </strong>' .$build_os. '</div>';
					echo '<div class="compare_features"><strong>UI: </strong>' .$build_ui. '</div>';
					echo '<div class="compare_features"><strong>Dimentions: </strong>' .$build_dimensions. '</div>';
					echo '<div class="compare_features"><strong>Weight: </strong>' .$build_weight. '</div>';
					echo '<div class="compare_features"><strong>Sim: </strong>' .$build_sim. '</div>';
					echo '<div class="compare_features"><strong>Colors: </strong>' .$build_colors. '</div>';
					echo '<div class="compare_features"><strong>2G Band: </strong>' .$frequency_2g_band. '</div>';
					echo '<div class="compare_features"><strong>3G Band: </strong>' .$frequency_3g_band. '</div>';
					echo '<div class="compare_features"><strong>4G Band:</strong>' .$frequency_4g_band. '</div>';
					echo '<div class="compare_features"><strong>CPU:</strong>' .$processor_cpu. '</div>';
					echo '<div class="compare_features"><strong>Chipset:</strong>' .$processor_chipset. '</div>';
					echo '<div class="compare_features"><strong>Disp Tech:</strong>' .$display_technology. '</div>';
					echo '<div class="compare_features"><strong>Disp Size:</strong>' .$display_size. '</div>';
					echo '<div class="compare_features"><strong>Disp Res:</strong>' .$display_resolution. '</div>';
					echo '<div class="compare_features"><strong>Memory:</strong>' .$memory_built_in. '</div>';
					echo '<div class="compare_features"><strong>Cam Main:</strong>' .$cam_main. '</div>';
					echo '<div class="compare_features"><strong>Cam Features:</strong>' .$cam_features. '</div>';
					echo '<div class="compare_features"><strong>Cam Front:</strong>' .$cam_front. '</div>';
					echo '<div class="compare_features"><strong>Conn WLAN:</strong>' .$connectivity_wlan. '</div>';
					echo '<div class="compare_features"><strong>Conn Bluetooth:</strong>' .$connectivity_bluetooth. '</div>';
					echo '<div class="compare_features"><strong>Conn GPS:</strong>' .$connectivity_gps. '</div>';
					echo '<div class="compare_features"><strong>Conn USB:</strong>' .$connectivity_usb. '</div>';
					echo '<div class="compare_features"><strong>Conn NFC:</strong>' .$connectivity_nfc. '</div>';
					echo '<div class="compare_features"><strong>Conn Data:</strong>' .$connectivity_data. '</div>';
					echo '<div class="compare_features"><strong>Feat Sensors:</strong>' .$features_sensors. '</div>';
					echo '<div class="compare_features"><strong>Feat Audio:</strong>' .$features_audio. '</div>';
					echo '<div class="compare_features"><strong>Feat Browser:</strong>' .$features_browser. '</div>';
					echo '<div class="compare_features"><strong>Feat Messaging:</strong>' .$features_messaging. '</div>';
					echo '<div class="compare_features"><strong>Feat Games:</strong>' .$features_games. '</div>';
					echo '<div class="compare_features"><strong>Feat Touch:</strong>' .$features_torch. '</div>';
					echo '<div class="compare_features"><strong>Feat Extra:</strong>' .$features_extra. '</div>';
					echo '<div class="compare_features"><strong>Battery:</strong>' .$battery. '</div>';


				echo '</div>';
			break;

			case 'attribute':
				if ( $field_id === woodmart_get_opt( 'brands_attribute' ) ) {
					$brands = wc_get_product_terms( $product['id'], $field_id, array( 'fields' => 'all' ) );

					if ( empty( $brands ) ) {
						echo '-';
						return;
					}

					foreach ($brands as $brand) {
						$image = get_term_meta( $brand->term_id, 'image', true);

						if( ! empty( $image ) ) {
							echo '<div class="woodmart-compare-brand">';
								echo apply_filters( 'woodmart_image', '<img src="' . esc_url( $image ) . '" title="' . esc_attr( $brand->name ) . '" alt="' . esc_attr( $brand->name ) . '" />' );
							echo '</div>';
						} else {
							echo wp_kses_post( $product[ $field_id ] );
						}

					}
				} else {
					echo wp_kses_post( $product[ $field_id ] );
				}
				break;

			case 'weight':
				if ( $product[ $field_id ] ) {
					$unit = $product[ $field_id ] !== '-' ? get_option( 'woocommerce_weight_unit' ) : '';
					echo wc_format_localized_decimal( $product[ $field_id ] ) . ' ' . esc_attr( $unit );
				} 
				break;

			case 'description':
				echo apply_filters( 'woocommerce_short_description', $product[ $field_id ] );
				break;

			default:
				echo wp_kses_post( $product[ $field_id ] );
				break;
		}
	}
}


if ( ! function_exists( 'woodmart_get_compared_products_data' ) ) {
	/**
	 * Get compared products data
	 *
	 * @since 3.3
	 */
	function woodmart_get_compared_products_data() {
		$ids = woodmart_get_compared_products();

		if ( empty( $ids ) ) {
			return array();
		}

		$args = array(
			'include' => $ids,
		);

		$products = wc_get_products( $args );

		$products_data = array();

		$fields = woodmart_get_compare_fields();

		$fields = array_filter( $fields, function(  $field ) {
			return 'pa_' === substr( $field, 0, 3 );
		}, ARRAY_FILTER_USE_KEY );

		$divider = '-';

		foreach ( $products as $product ) {
			$rating_count = $product->get_rating_count();
			$average = $product->get_average_rating();

			$products_data[ $product->get_id() ] = array(
				'basic' => array(
					'title' => $product->get_title() ? $product->get_title() : $divider,
					'image' => $product->get_image() ? $product->get_image() : $divider,
					'rating' => wc_get_rating_html( $average, $rating_count ),
					'price' => $product->get_price_html() ? $product->get_price_html() : $divider,
					'add_to_cart' => woodmart_compare_add_to_cart_html( $product ) ? woodmart_compare_add_to_cart_html( $product ) :$divider,
				),
				'id' => $product->get_id(),
				'image_id' => $product->get_image_id(),
				'permalink' => $product->get_permalink(),
				'dimensions' => wc_format_dimensions( $product->get_dimensions( false ) ),
				'description' => $product->get_short_description() ? $product->get_short_description() : $divider,
				'weight' => $product->get_weight() ? $product->get_weight() : $divider,
				'sku' => $product->get_sku() ? $product->get_sku() : $divider,
				'availability' => woodmart_compare_availability_html( $product ),
			);

			foreach ( $fields as $field_id => $field_name ) {
				if ( taxonomy_exists( $field_id ) ) {
					$products_data[ $product->get_id() ][ $field_id ] = array();
					$terms = get_the_terms( $product->get_id(), $field_id );
					if ( ! empty( $terms ) ) {
						foreach ( $terms as $term ) {
							$term = sanitize_term( $term, $field_id );
							$products_data[ $product->get_id() ][ $field_id ][] = $term->name;
						}
					} else {
						$products_data[ $product->get_id() ][ $field_id ][] = '-';
					}
					$products_data[ $product->get_id() ][ $field_id ] = implode( ', ', $products_data[ $product->get_id() ][ $field_id ] );
				}
			}
		}

		return $products_data;
	}
}

if ( ! function_exists( 'woodmart_compare_availability_html' ) ) {
	/**
	 * Get product availability html.
	 *
	 * @since 3.3
	 */
	function woodmart_compare_availability_html( $product ) {
		$html         = '';
		$availability = $product->get_availability();

		if( empty( $availability['availability'] ) ) {
			$availability['availability'] = __( 'In stock', 'woocommerce' );
		}

		if ( ! empty( $availability['availability'] ) ) {
			ob_start();

			wc_get_template( 'single-product/stock.php', array(
				'product'      => $product,
				'class'        => $availability['class'],
				'availability' => $availability['availability'],
			) );

			$html = ob_get_clean();
		}

		return apply_filters( 'woocommerce_get_stock_html', $html, $product );
	}
}


if ( ! function_exists( 'woodmart_compare_add_to_cart_html' ) ) {
	/**
	 * Get product add to cart html.
	 *
	 * @since 3.3
	 */
	function woodmart_compare_add_to_cart_html( $product ) {
		if ( ! $product ) return;

		$defaults = array(
			'quantity'   => 1,
			'class'      => implode( ' ', array_filter( array(
				'button',
				'product_type_' . $product->get_type(),
				$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
				$product->supports( 'ajax_add_to_cart' ) && $product->is_purchasable() && $product->is_in_stock() ? 'ajax_add_to_cart' : '',
			) ) ),
			'attributes' => array(
				'data-product_id'  => $product->get_id(),
				'data-product_sku' => $product->get_sku(),
				'aria-label'       => $product->add_to_cart_description(),
				'rel'              => 'nofollow',
			),
		);

		$args = apply_filters( 'woocommerce_loop_add_to_cart_args', $defaults, $product );

		if ( isset( $args['attributes']['aria-label'] ) ) {
			$args['attributes']['aria-label'] = strip_tags( $args['attributes']['aria-label'] );
		}

		return apply_filters( 'woocommerce_loop_add_to_cart_link', 
			sprintf( '<a href="%s" data-quantity="%s" class="%s add-to-cart-loop" %s><span>%s</span></a>',
				esc_url( $product->add_to_cart_url() ),
				esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
				esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
				isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
				esc_html( $product->add_to_cart_text() )
			),
		$product, $args );
	}
}





if ( ! function_exists( 'woodmart_compare_available_fields' ) ) {
	/**
	 * All available fields for Theme Settings sorter option.
	 *
	 * @since 3.3
	 */
	function woodmart_compare_available_fields( $new = false) {
		$product_attributes = array();

		if( function_exists( 'wc_get_attribute_taxonomies' ) ) {
			$product_attributes = wc_get_attribute_taxonomies();
		}

		if ( $new ) {
			$options = array(
				'description' => array(
					'name'  => esc_html__( 'Description', 'woodmart' ),
					'value' => 'description',
				),
				'dimensions' => array(
					'name'  => esc_html__( 'Dimensions', 'woodmart' ),
					'value' => 'dimensions',
				),
				'weight' => array(
					'name'  => esc_html__( 'Weight', 'woodmart' ),
					'value' => 'weight',
				),
				'availability' => array(
					'name'  => esc_html__( 'Availability', 'woodmart' ),
					'value' => 'availability',
				),
				'sku' => array(
					'name'  => esc_html__( 'Sku', 'woodmart' ),
					'value' => 'sku',
				),

			);

			if ( count( $product_attributes ) > 0 ) {
				foreach ( $product_attributes as $attribute ) {
					$options[ 'pa_' . $attribute->attribute_name ] = array(
						'name'  => $attribute->attribute_label,
						'value' => 'pa_' . $attribute->attribute_name,
					);
				}	
			}
			return $options;
		}

		$fields = array(
			'enabled'  => array(
				'description' => esc_html__( 'Description', 'woodmart' ),
				'sku' => esc_html__( 'Sku', 'woodmart' ),
				'availability' => esc_html__( 'Availability', 'woodmart' ),
			),
			'disabled' => array(
				'weight' => esc_html__( 'Weight', 'woodmart' ),
				'dimensions' => esc_html__( 'Dimensions', 'woodmart' ),
			)
		);

		if ( count( $product_attributes ) > 0 ) {
			foreach ( $product_attributes as $attribute ) {
				$fields['disabled'][ 'pa_' . $attribute->attribute_name ] = $attribute->attribute_label;
			}	
		}

		return $fields;
	}
}


